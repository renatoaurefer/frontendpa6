import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Store from './store'
Vue.use(Router)


var get = Store.getters
let shieldNotLogged = (to, from, next) => {
  
  if (get.gettoken) {
    next()
  } else {
    window.alert('not authorized - Not logged')
    next('login')
  }
}

let shieldCap=(to, from, next) => {

  if (get.iscap) {
    next()
  } else {
    window.alert('not authorized - Not Captain')
    if (get.gettoken) {
      next('dash')
    } else {
      next('login')
    }
    
  }


}

let shieldADM=(to, from, next) => {

  if (get.isadm) {
    next()
  } else {
    window.alert('not authorized - Not ADM')
    if (get.gettoken) {
      next('dash')
    } else {
      next('login')
    }
  }


}

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      beforeEnter: (to, from, next) => {
        if (get.gettoken) {
          next('dash')
        } else {
          next('login')
        }
      }
    },
    {
      path: '/about',
      name: 'about',
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('./views/login.vue')
    },
    {
      path: '/facens',
      name: 'facens',
      beforeEnter() { location.href = "https://www.facens.br/inovacao/lince-facens-laboratorio-de-inovacao-e-competicoes-de-engenharia" }
    }, {
      path: '/cadastropessoa',
      name: "equipe",
      component: () => import('./views/cadastropessoa.vue'),
      beforeEnter:shieldADM
    }, {
      path: '/cadastroequipe',
      name: 'pa',
      component: () => import('./views/cadastroTeam.vue'),
      beforeEnter:shieldADM
    }, {
      path: '/pedidos',
      name: 'pedidos',
      component: () => import('./views/pedidos'),
      beforeEnter:shieldNotLogged
    }, {
      path: '/dash',
      name: 'dash',
      component: () => import('./views/dash'),
      beforeEnter:shieldNotLogged
    },{
      path:'/pessoacap',
      name:'pessoacap',
      component:()=>import('./views/cadastropessoacap'),
      beforeEnter:shieldCap
    }
  ]
})

