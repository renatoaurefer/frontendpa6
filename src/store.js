import Vue from 'vue'
import Vuex from 'vuex'
import * as Cookies from 'js-cookie'
import { log } from 'util'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: null,
    username: null,
    loading: false,
    itens: [],
    capitan: false,
    adm: false,
    IdUser:null,
    idEquipe:null
  },
  mutations: {
    setusername(state, name) {
      state.username = name
    },
    setToken(state, payload) {
      state.token = payload
    },
    setLoading(state, payload) {
      state.loading = payload
    },
    additem(state, payload) {
      state.itens.push(payload)
    },
    removeitem(state, payload) {
      state.itens.splice(payload, 1)
    },
    clear(state) {
      state.username = null
      state.token = null
      state.adm = false
      state.capitan = false
      state.IdUser = false
      state.idEquipe=null
    },
    setAdm(state, payload) {
      state.adm = payload
    },
    setCapitan(state, payload) {
      state.capitan = payload
    },
    setID(state, payload) {
      state.IdUser = payload
    }
  },
  actions: {
    setUser({ commit }, payload) {
      commit('setusername', payload.value)
      Cookies.set('username', payload.value, { expires: payload.exp })
    },
    setToken({ commit }, payload) {
      commit('setToken', payload.value)
      Cookies.set('access_token', payload.value, { expires: payload.exp })
    },
    setLoading({ commit }, payload) {
      commit('setLoading', payload)
    },
    addItem({ commit }, payload) {
      commit('addItem', payload)
    },
    removeitem({ commit }, payload) {
      commit('removeitem', payload)
    },
    logout({ commit }) {
      commit('clear')
    },
    setAdm({ commit }, payload) {
      commit('setAdm', payload.value)
      Cookies.set('is_adm', payload.value, { expires: payload.exp })
    },
    setCapitan({ commit }, payload) {
      commit('setCapitan', payload.value)
      Cookies.set('is_cap', payload.value, { expires: payload.exp })
    }
    ,
    setID({ commit }, payload) {
      commit('setID', payload.value)
      Cookies.set('IdUser', payload.value, { expires: payload.exp })
    },
    setIDEquipe({ commit }, payload) {
      commit('setIDEquipe', payload.value)
      Cookies.set('IdEquipe', payload.value, { expires: payload.exp })
    }

  },
  getters: {
    gettoken(state) {
      if (state.token == null) {
        state.token = Cookies.get('access_token')
      }
      return state.token
    },
    getusername(state) {
      if (state.username == null) {
        state.username = Cookies.get('username')
      }
      return state.username
    },
    getloading(state) {
      return state.loading
    },
    getItens(state) {
      return state.itens
    },
    isadm(state) {

      return state.adm || Cookies.get('is_adm') == 'true'
    },
    iscap(state) {
      return state.capitan || Cookies.get('is_cap') == 'true'
    },
    idUser(state){
      if (state.IdUser == null) {
        state.IdUser = Cookies.get('IdUser')
      }
      return state.IdUser
    },
    idEquipe(state){
      if (state.idEquipe == null) {
        state.idEquipe = Cookies.get('IdEquipe')
      }
      return state.idEquipe
    }

  }
})
